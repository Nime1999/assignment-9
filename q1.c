#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 100

int main ()
{
   FILE *fpointerW, *fpointerR, *fpointerA;
   char content[MAX_SIZE];

   fpointerW = fopen("assignment9.txt","w");
   fprintf(fpointerW,"UCSC is one of the leading institutes in Sri Lanka for computing studies.");
   fclose(fpointerW);

   fpointerR = fopen("assignment9.txt","r");
   while(!feof(fpointerR))
   {
       fgets(content, MAX_SIZE, fpointerR);
       puts(content);
   }
   fclose(fpointerR);

   fpointerA = fopen("assignment9.txt","a");
   fprintf(fpointerA,"\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
   fclose(fpointerA);

   return 0;
}
